# Flux CD
[Flux](https://fluxcd.io/) is a tool for keeping Kubernetes clusters in sync with sources of configuration (like Git repositories and OCI artifacts), and automating updates to configuration when there is new code to deploy.


## Flux v2

https://fluxcd.io/flux/installation/

### Flux CD Installation

**1. Install fluxctl CLI**
```
# download & install
curl -s https://fluxcd.io/install.sh | sudo bash

# bash autocompletion
. <(flux completion bash)


# verify installation
flux -v 
flux version
```


**2. Generate GitLab Access Token**
GitLab > Account > Preferences > Access Tokens > Generate new one

**3. Variables**
- export Gitlab Variables
```
export GITLAB_USER=pety.barczi
export GITLAB_TOKEN=glpat-a6N8Svso4A1eYx8gA5JX
```

**4. Flux checks**
- check prerequisities
```
flux check
flux check --pre
```

Output should look like:
```
root@master:~# flux check --pre
► checking prerequisites
✔ Kubernetes 1.25.6+k3s1 >=1.20.6-0
✔ prerequisites checks passed
```

**5. Install Flux components**
```
# For testing purposes you can install the Flux controllers without storing their manifests in a Git reposit
flux install


# with deployment of flux components in GitOps manner
flux bootstrap
```

**5. Check Flux deployment**
```
kubectl get all -n flux-system
```

```
root@master:~# flux check
► checking prerequisites
✔ Kubernetes 1.25.6+k3s1 >=1.20.6-0
► checking controllers
✔ notification-controller: deployment ready
► ghcr.io/fluxcd/notification-controller:v0.31.0
✔ helm-controller: deployment ready
► ghcr.io/fluxcd/helm-controller:v0.29.0
✔ kustomize-controller: deployment ready
► ghcr.io/fluxcd/kustomize-controller:v0.33.0
✔ source-controller: deployment ready
► ghcr.io/fluxcd/source-controller:v0.34.0
► checking crds
✔ alerts.notification.toolkit.fluxcd.io/v1beta2
✔ buckets.source.toolkit.fluxcd.io/v1beta2
✔ gitrepositories.source.toolkit.fluxcd.io/v1beta2
✔ helmcharts.source.toolkit.fluxcd.io/v1beta2
✔ helmreleases.helm.toolkit.fluxcd.io/v2beta1
✔ helmrepositories.source.toolkit.fluxcd.io/v1beta2
✔ kustomizations.kustomize.toolkit.fluxcd.io/v1beta2
✔ ocirepositories.source.toolkit.fluxcd.io/v1beta2
✔ providers.notification.toolkit.fluxcd.io/v1beta2
✔ receivers.notification.toolkit.fluxcd.io/v1beta2
✔ all checks passed
```

**6. Bootstrap Flux**
```
flux bootstrap gitlab \
  --owner=$GITLAB_USER \
  --repository=flux-example-x \
  --branch=main \
  --path=./clusters/my-cluster \
  --personal \
  --token-auth
```

>NOTE: provided repository will be created in GitLab


```
export GIT_PASSWORD=<YOURPASSWORD>
flux bootstrap git --url=https://gitlab.com/opensovereigncloud/storage/landscape.git --path=./clusters/storage-cluster-1 --username=pety.barczi --token-auth
```

**5. Uninstall Flux**
```
flux uninstall
```
NOTE: by default after uninstalling of flux all deployed resources stay in place, to change this behaviour you need to set for the bootstrap Kustomization:

```
prune: true
```
<br></br>

**FLUX Commands**
```
flux check --pre
flux check

flux -v 
flux version

flux get all -A
flux get sources -A 
flux get kustomization
flux logs -A

flux stats

flux tree kustomization infrastructure

flux uninstall
```

**More**
https://fluxcd.io/flux/cheatsheets/troubleshooting/

